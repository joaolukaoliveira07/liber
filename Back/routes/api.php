<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::post('book', [BookController::class, 'createBook']);
Route::put('book/{id}', [BookController::class, 'updateBook']);
Route::get('book', [BookController::class, 'readBooks']);
Route::get('book/{id}', [BookController::class, 'readBook']);
Route::delete('book/{id}', [BookController::class, 'deleteBook']);



Route::post('users', [UserController::class, 'createUser']);
Route::put('users/{id}', [UserController::class, 'updateUser']);
Route::get('users', [UserController::class, 'readUsers']);
Route::get('users/{id}', [UserController::class, 'readUser']);
Route::delete('users/{id}', [UserController::class, 'deleteUser']);



Route::put('postBook/{id}/{bookId}', [UserController::class, 'post']);
Route::put('changeUser/{id}/{bookId}', [UserController::class, 'removeUser']);


