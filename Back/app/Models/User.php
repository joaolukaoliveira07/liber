<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Http\Request;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function createUser(Request $request){
        $this->name = $request->name;
        $this->email = $request->email;
        $this->telephone = $request->telephone;
        $this->address = $request->address;
        $this->birth_date = $request->birth_date;
        $this->password = $request->password;
        $this->save();
    }
    public function updateUser(Request $request){
        if ($request->name){
            $this->name = $request->name;  
        }
        if ($request->email){
            $this->email = $request->email;
        }
        if ($request->telephone){
            $this->telephone = $request->telephone;
        }
        if ($request->address){
            $this->address = $request->address;
        }
        if ($request->birth_date){
            $this->birth_date = $request->birth_date;
        }
        if ($request->password){
            $this->password = $request->password;
        }
        $this->save();

    }
    
    public function books(){
        return $this->hasMany('App\Models\Book');
      }
    
}

