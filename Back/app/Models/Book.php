<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Book extends Model
{
    public function createBook(Request $request){
        $this->name = $request->name;
        $this->category = $request->category;
        $this->price = $request->price;
        $this->resume = $request->resume;
        $this->condition = $request->condition;
        $this->author = $request->author;
        $this->publisher = $request->publisher;
        $this->save();
    }

    public function updateBook(Request $request){
        if ($request->name){
            $this->name = $request->name;  
        }
        if ($request->category){
            $this->category = $request->category;
        }
        if ($request->price){
            $this->price = $request->price;
        }
        if ($request->resume){
            $this->resume = $request->resume;
        }
        if ($request->condition){
            $this->condition = $request->condition;
        }
        if ($request->author){
            $this->author = $request->author;
        }
        if ($request->publisher){
            $this->publisher = $request->publisher;
        }
        $this->save();
    }
  
    public function user(){
        return $this->belongsTo('App\Models\User');
      }
    
    
}