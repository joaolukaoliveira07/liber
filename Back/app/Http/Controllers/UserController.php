<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Book;

class UserController extends Controller
{
    public function createUser(Request $request){
        $user = new User;
        $user->createUser($request);
        return response()->json(['Sucesso' => $user]);
    }

    public function updateUser(Request $request, $id){
        $user = User::find($id);
        $user->updateUser($request);
        return response()->json(['Sucesso' => $user]);
    }

    public function readUsers (Request $request){
        $user = User::all();
        return response()->json(['Sucesso' => $user]);
    }
    public function readUser (Request $reques, $id){
        $user = User::find($id);
        return response()->json(['Sucesso' => $user]);
    }
    public function deleteUser (Request $request, $id){
        User::destroy($id);
        return response()->json(['Sucesso']);
    }

    public function post($user_id, $book_id){
        $book = Book::find($book_id);
        $book->user_id = $user_id;
        return response()->json([$book, 200]);
    }
    public function removeUser($user_id, $book_id){
        $book = Book::find($book_id);
        $book->user_id = null;
        return response()->json([$book, 200]);
    }
}
