<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;

class BookController extends Controller
{
    public function createBook(Request $request){
        $book = new Book;
        $book->createBook($request);
        return response()->json(['Sucesso' => $book]);
    }

    public function updateBook(Request $request, $id){
        $book = Book::find($id);
        $book->updateBook($request);
        return response()->json(['Sucesso' => $book]);
    }

    public function readBooks (Request $request){
        $book = Book::all();
        return response()->json(['Sucesso' => $book]);
    }
    public function readBook (Request $reques, $id){
        $book = Book::find($id);
        return response()->json(['Sucesso' => $book]);
    }
    public function deleteBook (Request $request, $id){
        Book::destroy($id);
        return response()->json(['Sucesso']);
    }
    
}
